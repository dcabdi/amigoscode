import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class hw11 {
    public static void main(String[] args) {
        List<Integer> arrayList1 = new ArrayList<Integer>(Arrays.asList(-2, -4 , 5, -3));
        List<Integer> arrayList2 = new ArrayList<Integer>(Arrays.asList(-2, -4 , 5, -3));
        List<Integer> arrayList3 = new ArrayList<Integer>();

        arrayList3.addAll(arrayList1);
        arrayList3.addAll(arrayList2);
        System.out.println(arrayList3);
        System.out.println("Minimum integer in array is : " + Collections.min(arrayList3));
    }
}
