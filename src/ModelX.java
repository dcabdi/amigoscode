public class ModelX extends Car {
    boolean autoPilot =  false;
    public void switchAutoPilotOn() {
        autoPilot = true;
        System.out.println("Auto Pilot is switched on");
    }
    public void switchAutoPilotOff() {
        autoPilot = false;
        System.out.println("Auto Pilot is switched off!");

    }
    public String returnCarModel() {
        return "The car model is ModelX";
    }
}
