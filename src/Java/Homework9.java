package Java;

import org.w3c.dom.Text;

public class Homework9 {
    public static void main(String[] args) {

        BaseElement TextElement = new BaseElement(); // Creating 1st object
        BaseElement LinkElement = new BaseElement(); // Creating 2nd object

        System.out.println(TextElement.userid); //
        System.out.println(TextElement.Click());  //Polymorphism
        System.out.println(TextElement.Click2()); // Polymorphism

        // LinkElement & TextElement both Inherit BaseElement //

    }
}
