import java.util.Objects;

public class Car {

    int odometer = 0;
    String color = "";

    public void drive(int miles) { // Method to drive
        System.out.println("The car drove " + miles + " miles.");
        odometer = odometer + miles;
    }

    public String returnCarModel() { // Polymorphism can be used in different classes and used the same //
        return "Car model is unknown!";
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        System.out.println("Setting your car color to " + color + ". ");
        if(Objects.equals(color, "")) {
            return;
        }
        this.color = color;
    }


}
// Encapsulation Public : Access identifier who can have access to this class. //
