package abstractionClass;

public class Balance {

        private int balance;
        private int depositAmount;
        private long amt;

        public void Deposit(int sum) {
            int depositAmount;
            if (sum != 0) {
                balance = balance + sum;
                System.out.println("You deposited $" + sum);
            }
        }

        public void Withdraw(long amt) {
            if (balance >= amt){
                System.out.println("You withdrew $ " + amt);
                balance = (int) (balance - amt);
                System.out.println("Your balance has changed. " + "Your current balance is $" + balance + ".");
            } else {
                System.out.println();
                System.out.println("Your balance is less than $" + amt + " Transaction Failed! ");
            }
        }

        public int getBalance(int sum) {
            balance = depositAmount + balance;
            System.out.println("Your balance has changed. " + "Your current balance is $" + balance + ".");
            return balance;
        }

        public int setBalance(int sum) {return balance;}

}

